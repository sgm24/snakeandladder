//: Playground - noun: a place where people can play

import UIKit

var players: [String:Int] = ["AAA" : 0,
                             "BBB" : 0,
                             "CCC" : 0]
var endPosition: Int = 100
var currentRound: Int = 1
var diceRoll: Int = 0
var hasWinner: Bool = false
var actionTiles: [Int : Int] = [5 : 13, 30 : -10, 38 : 10, 50 : -8, 60 : 13, 75 : -10, 89 : 5]


//Function:: Roll the dice.
func rollYourDice() -> Int {
    diceRoll = Int(arc4random_uniform(6)) + 1
    print("Dice Roll : \(diceRoll)")
    return diceRoll
}

//Loops until there is a winner
while (!hasWinner) {
    
    print("\n ROUND : \(currentRound) \n")
    for (player, currentPosition) in players {
        
        print("-Player \(player)-")
        //Roll the dice
        rollYourDice()

        var thisPlayerPosition:Int = currentPosition + diceRoll
        print("Player \(player) in position : \(thisPlayerPosition)")
        if let action: Int = actionTiles[thisPlayerPosition] {
            if action > 0 {
                print("Player landed on ladder : Climb up \(action) steps.")
            } else {
                print("Player landed on snake : Slide down \(abs(action)) steps.")
            }
            thisPlayerPosition += action
        }
        
        //Prints player's current position
        print("Player \(player) in position : \(thisPlayerPosition)")
        players[player] = thisPlayerPosition
        
        if thisPlayerPosition >= 100 {
            hasWinner = true
            print("\n\n ~~~ PLAYER \(player) HAS WON!! ~~~")
            break
        }
    }
    currentRound++
    print("=================")
}


